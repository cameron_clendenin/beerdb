//
//  ViewController.m
//  BeerDB
//
//  Created by Cam Clendenin on 10/11/14.
//  Copyright (c) 2014 Cam Clendenin. All rights reserved.
//

#import "ViewController.h"
#import "RESTMan.h"
#import "Beer.h"
#import "BeerDetailVC.h"
#import "SegueIdentifiers.h"

// Delay (seconds) until remote query should be performed.
static NSTimeInterval searchQueryDelay = 0.8f;

// Beers API call requires 2 or more chars.
static NSInteger minimumQueryLength = 2;

@interface ViewController ()

@property (nonatomic, strong) NSMutableDictionary *searchResultsCache;
@property (nonatomic, strong) NSString *currentSearchQuery;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.rowHeight = 58.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    _searchResultsCache = nil;
}


- (NSMutableDictionary *)searchResultsCache {
    if (_searchResultsCache) return _searchResultsCache;
    
    _searchResultsCache = [NSMutableDictionary dictionary];
    return _searchResultsCache;
}

#pragma mark -
#pragma mark - Network Logic

- (void)performNewSearchIfNecessaryWithQuery:(NSString *)query {
    if (query.length < minimumQueryLength) return;

    // if query text hasn't changed, fire a search.
    if ([query isEqualToString:_currentSearchQuery]) {
        [self performSearchWithQuery:query];
    }
}

- (void)performSearchWithQuery:(NSString *)query {
    // fire remote query.
    NSDictionary *queryParams = @{@"query": query};
    
    [RESTMan getObjectsOfType:BEER parameters:queryParams success:^(id responseData) {
        
        NSMutableArray *results = [NSMutableArray array];
        NSArray *rawBeerResponse = [responseData objectForKey:@"beers"];
        for (id b in rawBeerResponse) {
            Beer *beer = [[Beer alloc] initWithDictionary:b];
            [results addObject:beer];
        }
        
        if (![self.searchResultsCache objectForKey:query])
            [self.searchResultsCache setObject:results forKey:query];
        
        _beerResults = results;
        [self.tableView reloadData];
        
    } failure:^(NSString *errorMessage) {
        NSLog(@"Failed to get results for query : %@", query);
    }];
}


#pragma mark -
#pragma mark - Search Bar Delegate

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        [searchBar resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if ([searchText isEqualToString:@""]) {
        _beerResults = nil;
        [self.tableView reloadData];
        return;
    };
    
    __block NSString *query = searchText;
    
    if ([self.searchResultsCache objectForKey:query]) {
        _beerResults = [self.searchResultsCache objectForKey:query];

        [self.tableView reloadData];
        return;
    }
    
    _currentSearchQuery = query;
    [self performSelector:@selector(performNewSearchIfNecessaryWithQuery:) withObject:query afterDelay:searchQueryDelay];
}

#pragma mark -
#pragma mark - Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _beerResults.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifer = @"BeerCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifer];
    
    Beer *beer = [_beerResults objectAtIndex:indexPath.row];
    
    UILabel *lblBeerName = (UILabel *)[cell.contentView viewWithTag:1];
    lblBeerName.text = beer.name;
    
    UILabel *lblBreweryName = (UILabel *)[cell.contentView viewWithTag:2];
    lblBreweryName.text = beer.breweryName;
    
    UILabel *lblBeerABV = (UILabel *)[cell.contentView viewWithTag:3];
    lblBeerABV.text = beer.abv;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:kSegueBeerDetails sender:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:kSegueBeerDetails]) {
        Beer *selectedBeer = [_beerResults objectAtIndex:[_tableView indexPathForSelectedRow].row];
        
        BeerDetailVC *vc = segue.destinationViewController;
        vc.beer = selectedBeer;
    }
    
}



@end
