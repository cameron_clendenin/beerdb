//
//  BeerDetailVC.m
//  BeerDB
//
//  Created by Cam Clendenin on 10/11/14.
//  Copyright (c) 2014 Cam Clendenin. All rights reserved.
//

#import "BeerDetailVC.h"
#import "RESTMan.h"
#import "SegueIdentifiers.h"

@interface BeerDetailVC ()

@end

@implementation BeerDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateFields];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateFields {
    _lblBeerName.text = _beer.name;
    _lblBeerABV.text = _beer.abv;
    _lblBeerDescription.text = _beer.beerDescription;
    _lblBreweryName.text = _beer.breweryName;
    
    [_btnBreweryUrl setTitle:_beer.breweryUrl forState:UIControlStateNormal];
}

- (void)setBeer:(Beer *)beer {
    if (_beer != beer) {
        _beer = beer;
        
        if (!_beer.breweryUrl) {
            [RESTMan getObjectOfType:BREWERY withID:[_beer.breweryId stringByAppendingString:@".json"] parameters:nil success:^(id responseData) {
                _beer.breweryUrl = [responseData objectForKey:@"url"];
                [self updateFields];
            } failure:^(NSString *errorMessage) {
                
            }];
        }
    }
}

#pragma mark -
#pragma mark - User Actions

- (IBAction)breweryUrlButtonPressed:(id)sender {
    if (_beer.breweryUrl) {
        [self performSegueWithIdentifier:kSegueBreweryWebsite sender:nil];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSegueBreweryWebsite]) {
        UIViewController *vc = segue.destinationViewController;
        UIWebView *webView = (UIWebView *)[vc.view viewWithTag:1];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_beer.breweryUrl]];
        [webView loadRequest:request];
    }
}

@end
