//
//  ViewController.h
//  BeerDB
//
//  Created by Cam Clendenin on 10/11/14.
//  Copyright (c) 2014 Cam Clendenin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *beerResults;

@end

