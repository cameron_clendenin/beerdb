//
//  BeerDetailVC.h
//  BeerDB
//
//  Created by Cam Clendenin on 10/11/14.
//  Copyright (c) 2014 Cam Clendenin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Beer.h"

@interface BeerDetailVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblBeerName;
@property (weak, nonatomic) IBOutlet UILabel *lblBeerABV;
@property (weak, nonatomic) IBOutlet UILabel *lblBeerDescription;

@property (weak, nonatomic) IBOutlet UILabel *lblBreweryName;
@property (weak, nonatomic) IBOutlet UIButton *btnBreweryUrl;

@property (weak, nonatomic) Beer *beer;

@end
