//
//  Beer.m
//  BeerDB
//
//  Created by Cam Clendenin on 10/11/14.
//  Copyright (c) 2014 Cam Clendenin. All rights reserved.
//

#import "Beer.h"

@implementation Beer

- (instancetype)initWithDictionary:(NSDictionary *)info {
    self = [super init];
    if (self) {
        [self updateWithDictionary:info];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)info {
    if (!_beerId)
        _beerId = [[info valueForKey:@"id"] stringValue];
    

    if (!_name)
        _name = [info objectForKey:@"name"];
    
    if (!_beerDescription)
        _beerDescription = [info objectForKey:@"description"];
    
    if (!_abv)
        _abv = [[NSString stringWithFormat:@"%.1f", [[info valueForKey:@"abv"] floatValue]] stringByAppendingString:@"%"];
    
    NSDictionary *breweryInfo = [info objectForKey:@"brewery"];
    if (!_breweryId)
        _breweryId = [[breweryInfo valueForKey:@"id"] stringValue];

    if (!_breweryName) {
        _breweryName = [breweryInfo objectForKey:@"name"];
    }
}

@end
