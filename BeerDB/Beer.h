//
//  Beer.h
//  BeerDB
//
//  Created by Cam Clendenin on 10/11/14.
//  Copyright (c) 2014 Cam Clendenin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Beer : NSObject

@property (strong, nonatomic) NSString *beerId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *beerDescription;
@property (strong, nonatomic) NSString *abv;

// Brewery info
@property (strong, nonatomic) NSString *breweryId;
@property (strong, nonatomic) NSString *breweryName;
@property (strong, nonatomic) NSString *breweryUrl;

- (instancetype)initWithDictionary:(NSDictionary *)info;

- (void)updateWithDictionary:(NSDictionary *)info;

@end
