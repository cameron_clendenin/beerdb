//
//  SegueIdentifiers.h
//  BeerDB
//
//  Created by Cam Clendenin on 10/11/14.
//  Copyright (c) 2014 Cam Clendenin. All rights reserved.
//

#ifndef BeerDB_SegueIdentifiers_h
#define BeerDB_SegueIdentifiers_h

static NSString *kSegueBeerDetails = @"BeerDetailSegue";
static NSString *kSegueBreweryWebsite = @"BreweryWebsiteSegue";

#endif
